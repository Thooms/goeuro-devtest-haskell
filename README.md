
# How to run it

Assuming you have a working `stack` installation:

```
* stack build
* ./resulting-exec Berlin
* cat Berlin.csv
```
