{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import           Control.Lens ((^.))
import           Control.Monad (forM_)
import           Data.Aeson
import qualified Data.ByteString as BS
import           Data.List (intersperse)
import           GHC.Generics
import           Network.Wreq (get, responseBody)
import           System.Environment (getArgs)
import           System.IO

data Rec = Rec {
  _id :: Int
  , name :: String
  , _type :: String
  , latitude :: Float
  , longitude :: Float
  } deriving Show

instance FromJSON Rec where
  parseJSON (Object v) =
    Rec <$>
    v .: "_id" <*>
    v .: "name" <*>
    v .: "type" <*>
    ((v .: "geo_position") >>= (.: "latitude")) <*>
    ((v .: "geo_position") >>= (.: "longitude"))

  parseJSON _ = mempty

data Resp = Resp [Rec] deriving (Generic, Show)

instance FromJSON Resp

fetch :: String -> IO (Maybe Resp)
fetch city = do
  r <- get (baseURL ++ city)
  return $ (decode $ r ^. responseBody)
  where baseURL = "http://api.goeuro.com/api/v2/position/suggest/en/"

generateCSV city = do
  res <- fetch city
  case res of
    Nothing -> putStrLn "Unable to fetch data"
    Just (Resp l) ->
      withFile (city ++ ".csv") WriteMode $ \handle -> do
        handle `hPutStrLn` "_id,name,type,latitude,longitude"
        forM_ l $ \entry -> do
          sequence $ map (hPutStr handle)$ intersperse "," $ [
            show (_id entry)
            , name entry
            , _type entry
            , show (latitude entry)
            , show (longitude entry)
            ]
          handle `hPutStrLn` ""

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> putStrLn "Usage: ./goeuro-devtest city"
    (city:_) -> generateCSV city
